/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>
#include <stdlib.h>

#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
#define LED_NUCLEO144_LED1_PIN 		GPIO_Pin_0
#define LED_NUCLEO144_LED2_PIN 		GPIO_Pin_7
#define LED_NUCLEO144_LED3_PIN 		GPIO_Pin_14
#define LED_NUCLEO144_PORT 			GPIOB

/* Exported macro ------------------------------------------------------------*/

/* Exported functions ------------------------------------------------------- */
void vRCC_Init(void);
void vGPIO_Init(void);

void vEng_Cyclic( void *pvParameters );

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
