#include "main.h"

/**************************/

int main(void)
{
	vRCC_Init();

	/*******************/

	vGPIO_Init();

	/*******************/

	xTaskCreate( vEng_Cyclic, "Eng_PeriodicTask_1000ms", configMINIMAL_STACK_SIZE, NULL, ( tskIDLE_PRIORITY + 5U ), NULL );

	/*******************/

	vTaskStartScheduler();

	for(;;);

	return 0;
}

void vEng_Cyclic( void *pvParameters )
{
	TickType_t xLastWakeTime = xTaskGetTickCount();

	/*!!! NOT variables permitted in this space (after TickType_t and before for(;;) ) !!!*/

	for( ;; )
	{
		/* Wait for the next cycle */
		vTaskDelayUntil( &xLastWakeTime, pdMS_TO_TICKS(1000) );

		GPIO_ToggleBits(LED_NUCLEO144_PORT, LED_NUCLEO144_LED1_PIN);
		GPIO_ToggleBits(LED_NUCLEO144_PORT, LED_NUCLEO144_LED2_PIN);
		GPIO_ToggleBits(LED_NUCLEO144_PORT, LED_NUCLEO144_LED3_PIN);
	}
}

void vGPIO_Init(void)
{
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

	GPIO_InitTypeDef xGPIO;
	GPIO_StructInit(&xGPIO);

	xGPIO.GPIO_Mode = GPIO_Mode_OUT;
	xGPIO.GPIO_Speed = GPIO_Fast_Speed;
	xGPIO.GPIO_OType = GPIO_OType_PP;
	xGPIO.GPIO_PuPd = GPIO_PuPd_DOWN;
	xGPIO.GPIO_Pin = LED_NUCLEO144_LED1_PIN | LED_NUCLEO144_LED2_PIN | LED_NUCLEO144_LED3_PIN;

	GPIO_Init(LED_NUCLEO144_PORT, &xGPIO);

	GPIO_ResetBits(LED_NUCLEO144_PORT, LED_NUCLEO144_LED1_PIN);
	GPIO_ResetBits(LED_NUCLEO144_PORT, LED_NUCLEO144_LED2_PIN);
	GPIO_ResetBits(LED_NUCLEO144_PORT, LED_NUCLEO144_LED3_PIN);
}

void vRCC_Init(void) //Add HSE Config
{
	SysTick_Config( SystemCoreClock / 1000 );
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	//NVIC_SetPriority(SysTick_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 0U, 0U));

	/*************************/

	RCC_DeInit();

	RCC_HSICmd(ENABLE);
	while(RCC_GetFlagStatus(RCC_FLAG_HSIRDY) == RESET){};

	RCC_HCLKConfig(RCC_SYSCLK_Div1);
	RCC_PCLK1Config(RCC_HCLK_Div4);
	RCC_PCLK2Config(RCC_HCLK_Div2);
	//RCC_AdjustHSICalibrationValue(0x10);
	RCC_PLLConfig(RCC_PLLSource_HSI, 8, 180, 2, 4); //180MHz

	RCC_PLLCmd(ENABLE);
	while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET){};

	RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);

	/**************************/

	RCC_ClocksTypeDef RCC_BaseStruct;
	RCC_GetClocksFreq(&RCC_BaseStruct);

	/**************************/

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

	/*****************************/
}
